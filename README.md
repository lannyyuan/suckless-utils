# Matt's DWM Install

This repo includes all of my suckless utils, including my DWM builds with all of the themes I've created. Instructions for install will come soon. If you choose to use them now, note that the patched version of DWM requires a .dwm directory in your $HOME directory. You can find this in my scripts repo. https://gitlab.com/thelinuxcast/scripts

To change themes, change the first line of the config.def.h file before compile. Must be an absolute path. Theme folder is included inside dwm folder. Remember to change the username in the path!

The standard folders (dwm, slstatus) are for the new build that I'm working on. This build does not have a ton of ricing done. If you want my older dwm setup, the one with all the themes, you will need to use dwm.old and change the theme folder in slstatus from themes to themes.status2d, as the old dwm uses the status2d colorizer and the new one does not. 
